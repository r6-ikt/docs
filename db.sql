-- Table: history_operator
CREATE TABLE IF NOT EXISTS history_operator (
                                                id SERIAL PRIMARY KEY,
                                                operator_id INTEGER NOT NULL,
                                                history_date DATE DEFAULT CURRENT_DATE,

                                                FOREIGN KEY (operator_id) REFERENCES operators(operator_id)
    );

-- Table: history_user
CREATE TABLE IF NOT EXISTS history_user (
                                            id SERIAL PRIMARY KEY,
                                            user_id INTEGER NOT NULL,
                                            history_date DATE DEFAULT CURRENT_DATE,
                                            earned_points INTEGER,

                                            FOREIGN KEY (user_id) REFERENCES users(user_id)
    );

-- Table: issuehelper
CREATE TABLE IF NOT EXISTS issuehelper (
                                           issue_id SERIAL PRIMARY KEY,
                                           user_id INTEGER NOT NULL,
                                           send_date DATE DEFAULT CURRENT_DATE,
                                           done_date DATE,
                                           complaint TEXT,

                                           FOREIGN KEY (user_id) REFERENCES users(user_id)
    );

-- Table: operators
CREATE TABLE IF NOT EXISTS operators (
                                         operator_id SERIAL PRIMARY KEY,
                                         name VARCHAR(255) UNIQUE NOT NULL,
    side BOOLEAN,
    hp INTEGER,
    speed INTEGER,
    release_date INTEGER,
    nationality VARCHAR(255),
    weapon1 VARCHAR(255),
    weapon2 VARCHAR(255),
    squad VARCHAR(255),
    active BOOLEAN DEFAULT TRUE
    );

-- Table: users
CREATE TABLE IF NOT EXISTS users (
                                     user_id SERIAL PRIMARY KEY,
                                     username VARCHAR(255) UNIQUE NOT NULL,
    email VARCHAR(255) UNIQUE NOT NULL,
    password VARCHAR(255),
    streak INTEGER DEFAULT 0,
    point INTEGER DEFAULT 0,
    admin BOOLEAN DEFAULT FALSE,
    create_at DATE DEFAULT CURRENT_DATE,
    last_login DATE DEFAULT CURRENT_DATE,
    active BOOLEAN DEFAULT TRUE
    );

-- Table: guesses
CREATE TABLE IF NOT EXISTS guesses (
                                       user_id INTEGER,
                                       operator_id INTEGER,

                                       PRIMARY KEY (user_id, operator_id),
    FOREIGN KEY (user_id) REFERENCES users(user_id),
    FOREIGN KEY (operator_id) REFERENCES operators(operator_id)
    );

-- Table: guest_user
CREATE TABLE IF NOT EXISTS guest_user (
                                          guest_token VARCHAR(255) PRIMARY KEY
    );

-- Table: guest_guesses
CREATE TABLE IF NOT EXISTS guest_guesses (
                                             guest_token VARCHAR(255),
    operator_id INTEGER,

    PRIMARY KEY (guest_token, operator_id),
    FOREIGN KEY (guest_token) REFERENCES guest_user(guest_token),
    FOREIGN KEY (operator_id) REFERENCES operators(operator_id)
    );

-- Table: patch_note
CREATE TABLE IF NOT EXISTS patch_note (
                                          patch_id SERIAL PRIMARY KEY,
                                          operator_id INTEGER,
                                          admin_id INTEGER,
                                          updated_field VARCHAR(255),
    previous_value VARCHAR(255),
    new_value VARCHAR(255),
    patch_date DATE DEFAULT CURRENT_DATE,

    FOREIGN KEY (operator_id) REFERENCES operators(operator_id),
    FOREIGN KEY (admin_id) REFERENCES users(user_id)
    );
