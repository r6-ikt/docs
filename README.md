R6DLE
-

**A Rainbow Six Siege videojátékkal kapcsolatban tengermély ismeretet igénylő szórakoztató és gondolkodtató játék.**

Funkciók:
-

- Naponta frissülő új operátor
- Kompetitív játékmód
- Vendégként is játszhatsz
- Játékosok üzenhetnek adminoknak IssueHelperrel.
- Reszponzivitás
- Azonnali tájékoztatás bárminemű változásról

![R6dle](https://fbi.cults3d.com/uploaders/20470091/illustration-file/4a3bc3b5-3a54-4c8d-bdfa-0232fc950341/Rainbow-Six-Symbol.png)