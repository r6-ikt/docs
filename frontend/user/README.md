R6DLE Frontend felhasználói dokumentáció
-

Regisztráció:\
[register.mp4](videos/register.mp4)

Bejelentkezés:\
[login.mp4](videos/login.mp4)

Operátorok:\
[operators.mp4](videos/operators.mp4)

Tippelés:\
[guessing.mp4](videos/guessing.mp4)

Panasz küldése:\
[issuehelper.mp4](videos/issuehelper.mp4)

Profil módosítása:\
[profile_modification.mp4](videos/profile_modification.mp4)