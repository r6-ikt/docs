R6DLE Frontend fejlesztői dokumentáció
-

_Fejlesztői dokumentáció_

Fejlesztői környezet
-
---

A frontend futtatásához a következők kellenek:

- [Node js 19.0 vagy újabb](https://nodejs.org/en/download)

Futtatás
-
---

Futtatáshoz szükséges, hogy fusson a backend alkalmazás is! A package.json fájlban lehet beállítani, hogy hol éri el a
backendet.

```
"proxy": "http://localhost:3000/"
```

Fejlesztés
-

A frontend mappában az **npm start** paranccsal futtatható a react alkalmazás.

Frontend-backend összekapcsolás
-
---

Az **npm run build** paranccsal lehet egy buildet készíteni a frontendből. Majd a dist mappában található minden mappát
és fájlt kell átmásolni a backendbe.


Hasznos dokumentációk
-
---
[React](https://www.w3schools.com/REACT/DEFAULT.ASP)